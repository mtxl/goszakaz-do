def time(date_string)
  DateTime.strptime(date_string).strftime("%H:%M")
end
  
def number_to_currency(amount_string)
  n = amount_string.to_i.to_s
  "#{n.gsub(/(\d)(?=(\d\d\d)+(?!\d))/, "\\1 ")} руб."
end

def actual_kurses
  @item.children.sort_by do |i|
    i[:id]
  end
end

def event_date(start_at, end_at)
#  return "" unless start_at && end_at
  start_at_df = Date.strptime(start_at)
  end_at_df = Date.strptime(end_at)
  if start_at_df.day == end_at_df.day
    Russian::strftime( start_at_df, "%d %B %Y")
  else
    "#{start_at_df.day}-#{Russian::strftime(end_at_df,"%d %B %Y")}"
  end  

end

def actual_webinars
  @item.children.find_all do |i|
    i if Date.strptime(i[:start_at]) > Date.today
  end
end

def render_webinars_archive
  archive = @items.find_all { |i| i if i.identifier.match("/вебинары/архив/")}
  s = archive.inject("<ul>") do |m,i|
    identifier = "/вебинары/архив/#{i[:year]}/"
    m +="<li>#{link_to(i[:year].to_s, identifier)}</li>" 
  end
  if s
    "<h2> Архив вебинаров </h2>" + s + "</ul>"
  else
    ""
  end
end

