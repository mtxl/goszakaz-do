def create_webinars_archive
  item = @items.find {|i| i.identifier == '/вебинары/'}
  h = {}

  item.children.each do |i| 
    next if Date.strptime(i[:start_at]) > Date.today
    start_at = i[:start_at] || Time.now.to_s
    year = Date.strptime(start_at).strftime('%Y')
    h[year] ||= []
    h[year] << i
  end


  @archive_webinars = []
  h.keys.sort.each do |k| 
    attributes = {
      :title => "Архив вебинаров за #{k} год",
      :year  => k,
      :child => h[k]
    }
    archive_item = Nanoc3::Item.new("", attributes, "#{item.identifier}архив/#{k}")
    @items << archive_item
  end
end
