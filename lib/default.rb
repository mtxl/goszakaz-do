require "bundler/setup"
Bundler.require
# encoding: utf-8
# All files in the 'lib' directory will be loaded
# before nanoc starts compiling.
include Nanoc3::Helpers::LinkTo
include Nanoc3::Helpers::Breadcrumbs

#def top_level_items
#  a = [@items.find {|i| i.identifier == '/'}]
#  @items.each  do |i|
#    a << i if i.identifier.match(/^\/[^\/]+\/$/)
#  end
#  a
#end
#%ul
#  - top_level_items.each do |i|
#    - if i.identifier == @item.identifier 
#      %li.current_page_item
#        = link_to(i[:title], i.identifier)
#    - elsif i.identifier != '/' and @item.identifier.match(i.identifier)
#      %li.current_page_item
#        = link_to(i[:title], i.identifier)
#    - else
#      %li
#        = link_to(i[:title], i.identifier)

def render_navigation
  class_selected = @item.identifier == '/' ? 'class = "current_page_item"' : ''
  item_root = @items.find {|i| i.identifier == '/'}
  s = "<ul><li #{class_selected}>"+link_to(item_root[:title], item_root.identifier)+"</li>"   
  #%w(/новости/ /вебинары/ /курсы/ /как-проходит-обучение/ /отзывы-клиентов/ /обратная-связь/).each  do |i|
  %w(новости вебинары курсы как-проходит-обучение отзывы-слушателей обратная-связь).each  do |i|
      class_selected = @item.identifier.match("/#{i}/") ? 'class = "current_page_item"' : ''
      if item = @items.find {|item| item.identifier == "/#{i}/"} then
        s += "<li #{class_selected}>"+link_to( item[:menu_title] ? item[:menu_title] : i.gsub("-"," "), item.identifier)+"</li>"   
      end  
  end   
  s +="</ul>"
end

def date_sorted_news
  require 'date'
  fmt = '%d-%m-%Y'
  a = @item.children.map{ |i| Date.strptime(i.identifier.match(/\d\d-\d\d-\d\d\d\d/)[0],fmt)}.sort {|a,b| b <=> a }
  a.map {|a| @items.find {|i| i.identifier == "/новости/#{a.strftime(fmt)}/"}}
end
